python-django-babel (0.6.2-7) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090489).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 10:19:28 +0100

python-django-babel (0.6.2-6) unstable; urgency=medium

  * Add remove-intersphinx.patch (Closes: #1090140).

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Dec 2024 16:13:53 +0100

python-django-babel (0.6.2-5) unstable; urgency=medium

  * Define DJANGO_SETTINGS_MODULE when building docs (Closes: #978293).

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Jan 2021 08:04:08 +0100

python-django-babel (0.6.2-4) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Antonio Terceiro ]
  * debian/patches/Fix-For-Django2.patch: Add patch to fix string extraction
    with Django 2

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 24 Oct 2019 07:57:39 -0300

python-django-babel (0.6.2-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * Remove Python 2 support.
  * Support the nodoc build profile.
  * Standards-Version: 4.4.0.

 -- Thomas Goirand <zigo@debian.org>  Wed, 10 Jul 2019 11:12:01 +0200

python-django-babel (0.6.2-1) unstable; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

  [ Thomas Goirand ]
  * Fixed upstream VCS URL to new github repo.
  * New upstream release.
  * Bump debhelper to 10.
  * Using pkgos-dh_auto_install.
  * Fixed debian/copyright years and order.

 -- Thomas Goirand <zigo@debian.org>  Sat, 03 Mar 2018 19:04:34 +0100

python-django-babel (0.5.1-2) unstable; urgency=medium

  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.

 -- Ondřej Nový <onovy@debian.org>  Mon, 26 Sep 2016 19:09:02 +0200

python-django-babel (0.5.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Ivan Udovichenko ]
  * Package new release.
  * Add myself to uploaders field.

 -- Ivan Udovichenko <iudovichenko@mirantis.com>  Tue, 14 Jun 2016 13:35:11 +0300

python-django-babel (0.4.0-1) unstable; urgency=medium

  * Initial release. (Closes: #794487)

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Aug 2015 17:04:31 +0200
